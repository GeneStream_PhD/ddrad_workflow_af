## Python script used to filter Stacks loci according to minor allele frequency, minimum of individuals that should share the locus.
## Further, loci can be excluded which are invariable and/or have more than two alleles per locus and the first variable snp of a locus can be chosen.

## written by Hannah Weigand, modified by Martina Weiss, University of Duisburg-Essen
## used in Weiss et al 2017...

import re
import os
import shutil
import sys
import numpy as np


my_data = snakemake.input[0]
my_out1 = snakemake.output.o1
my_out2 = snakemake.output.o2
my_out3 = snakemake.output.o3
#my_out4 = snakemake.output.o4
tre_min_af = float(snakemake.wildcards.ma)
tre_indiv = float(snakemake.wildcards.lim2)
variable = snakemake.config["variable"]
two_all = snakemake.config["two_loci"]
only_one = snakemake.config["first_snp"]
title_line = snakemake.output.o1

#empty lists, tables, ...
i = 0
pop = 0
pop_sep = {}
ind_names = []
locus_counts = {}
ref_allele = []
alleles = {}
genotypes = {}


#read data and bring it to usable format
for line in open(my_data):
        i = i + 1
        if i == 1:
                title = line.rstrip("\n")
                title = str(title_line)
                m = re.search("/(.*).gen", title)
                if m:
                    new_title = m.group(1)
                    print(new_title)

        elif i == 2:
                locus_list = line.replace(" ","").rstrip("\n").split(",")

        else:
                line2 = line.replace(" ","").replace(",", "\t").rstrip("\n").split("\t")
                if len(line2) < 2:
                        pop = pop + 1
                else:
                        pop_sep[line2[0]]=[pop]+line2[1:]
                        ind_names = ind_names + [line2[0]]
        numb_pop = pop

#Summerize data
print(str(numb_pop)+ " population/s identified")


#Count alleles
j = 0
if locus_list[-1] == "":
        locus_list = locus_list[0:len(locus_list)-1]
for loc in locus_list:
        ref_allele = ref_allele + ["00"]
        alleles[j] = ["no"]
        genotypes[j] = ["0000"]
        j = j + 1


#Define reference allele
for ind in ind_names:
        i = 0
        loc_li = pop_sep.get(ind)[1:]

        if loc_li[-1] == "":
                loc_li = loc_li[0:len(loc_li)-1]

        if loc_li[0] == "":
                loc_li = loc_li[1:]

        for x in loc_li:
                 if x != "0000":
                        a1 = x[0:2]
                        a2 = x[2:]
                        ref = ref_allele[i]
                        alle = alleles.get(i)
                        if ref == "00":
                                ref_allele[i] = a1

                        if alle == ["no"]:
                                alleles[i] = [a1]
                                locus_counts[i] = [0]
                                if a1 != a2:
                                        alleles[i] = [a1] + [a2]
                                        locus_counts[i] = [0] + [0]

                        else:
                                if alle.count(a1) < 1:
                                        alle = alle + [a1]
                                        alleles[i] = alle
                                        loc_c = locus_counts.get(i)
                                        locus_counts[i] = loc_c + [0]
                                if alle.count(a2) < 1:
                                        alleles[i] = alle + [a2]
                                        loc_c = locus_counts.get(i)
                                        locus_counts[i] = loc_c + [0]

                 i = i + 1



#Count alleles:
for ind in ind_names:
        i = 0
        loc_li = pop_sep.get(ind)[1:]
        if loc_li[-1] == "":
                loc_li = loc_li[0:len(loc_li)-1]
        if loc_li[0] == "":
                loc_li = loc_li[1:]

        for x in loc_li:

                if x != "0000":
                        a1 = x[0:2]
                        a2 = x[2:]
                        ref = ref_allele[i]
                        alle = alleles.get(i)
                        lo_c = locus_counts.get(i)
                        k = 0
                        for a in alle:
                                if a1 == a:
                                        co = lo_c[k]
                                        lo_c[k] = co + 1

                                if a2 == a:
                                        co = lo_c[k]
                                        lo_c[k] = co + 1
                                k = k + 1


                        locus_counts[i] = lo_c
                i = i + 1

#Count genotypes:
for ind in ind_names:
    i = 0
    loc_li = pop_sep.get(ind)[1:]
    if loc_li[-1] == "":
        loc_li = loc_li[0:len(loc_li)-1]
    if loc_li[0] == "":
        loc_li = loc_li[1:]
    for x in loc_li:
        if x != "0000":
            a1 = x[0:2]
            a2 = x[2:]
            loc_alt = a2 + a1
            lo_c = locus_counts.get(i)
            geno = genotypes.get(i)
            if geno[0] == "0000":
                geno = [x]
                genotypes[i] = geno
            else:
                if (geno.count(x) < 1) and (geno.count(loc_alt) < 1):
                    geno = geno + [x]
                    genotypes[i] = geno
        i = i + 1

#Define output loci

status_list = []
out_ma = 0
out_in = 0
out_var = 0
out_two = 0
pop_sep2 = {}
#pop_sep3 = {}

for loc in range(0,len(locus_list)):
        counts = locus_counts.get(loc)
        geno = genotypes.get(loc)
        total = 0
        status = "ok"
        for x in counts:
                total = total + x
        thre_ma = np.ceil(total*tre_min_af*0.01)
        thre_in = np.ceil(len(ind_names)*tre_indiv*0.01)*2
        for x in counts:
                if x < thre_ma:
                        status = "no"
                        out_ma = out_ma + 1

        if total < thre_in:
                status = "no"
                out_in = out_in  + 1

        if variable == "y":
                if len(counts) == 1 or len(geno) < 2:
                    status = "no"
                    out_var = out_var + 1
        else:
                out_var = "not set"

        if two_all == "y":
                if len(counts) > 2:
                        status = "no"
                        out_two = out_two + 1
        else:
                out_two = "not set"


        status_list = status_list + [status]

#Idetify loci per individual
for ind in ind_names:
        s = 0
        loci_new = []
        # loci_not = []
        loci = pop_sep.get(ind)[1:]
        loci_new = [pop_sep.get(ind)[0]]
        # loci_not = [pop_sep.get(ind)[0]]

        for st in status_list:
                if st == "ok":
                        loci_new = loci_new + [loci[s]]
                #else:
                #        loci_not = loci_not + [loci[s]]
                s = s + 1

        pop_sep2[ind] = loci_new
        # pop_sep3[ind] = loci_not

#Identify locus names
li = 0
locus_list_ok = []
locus_list_not = []
for loc in locus_list:
        if status_list[li] == "ok":
                locus_list_ok = locus_list_ok + [loc]
        else:
                locus_list_not = locus_list_not + [loc]
        li = li + 1

#Identify first SNP per locus that passes all activated criteria
if only_one == "y":
    posi = 0
    multi = {}
    snp_pos = []
    for loc in locus_list_ok:
        snp_pos = snp_pos + [posi]
        posi = posi + 1
        se = loc.find("_")
        loc_name = loc[0:se]
        loc_pos = loc[se+1:]
        if list(multi.keys()).count(loc_name) < 1:
            multi[loc_name] = [loc_pos]
        else:
            old = multi.get(loc_name)
            new = old +  [loc_pos]
            new.sort()
            multi[loc_name] = new

    locus_list_ok_new = []
    for locus in multi.keys():
        loc_li = multi.get(locus)
        acc = str(locus) + "_" + str(loc_li[0])
        locus_list_ok_new = locus_list_ok_new + [acc]
    #print(locus_list_ok_new)

    status_new = []
    loci_names_ok = []
    loci_names_not = []
    #print(locus_list_ok)
    for loc in locus_list_ok:
            if locus_list_ok_new.count(loc) == 1:
                status_new = status_new + ["ok"]
                loci_names_ok = loci_names_ok + [loc]
            else:
                status_new = status_new + ["no"]
                loci_names_not = loci_names_not + [loc]
    #print(status_new)

    for ind in ind_names:
            s = 0
            loci_old = pop_sep2.get(ind)[1:]
            #loci_not_old = pop_sep3.get(ind)[1:]
            loci_new = [pop_sep2.get(ind)[0]]
            #loci_not = [pop_sep3.get(ind)[0]]

            for st in status_new:
                if st == "ok":
                     loci_new = loci_new + [loci_old[s]]
                # else:
                #      loci_not = loci_not + [loci_not_old[s]]
                s = s + 1

            pop_sep2[ind] = loci_new
            # pop_sep3[ind] = loci_not

    old_len = len(loci_names_ok)
    locus_list_ok = loci_names_ok
    locus_list_not = loci_names_not


#Write output
ind_names.sort()
output = open(my_out1, "w")
output = open(my_out1, "a")

#output_not = open(my_out4, "w")
#output_not = open(my_out4, "a")

output.write(new_title + "\n")
#output_not.write(title + "\n")

print(*locus_list_ok, sep=",", file=output) #Alternative zu Hannahs Befehl

#print(*locus_list_not, sep=",", file=output_not)


for p in range(0,numb_pop):
        output.write("POP\n")
        p2 = p + 1
        for individual in ind_names:
                list1 = pop_sep2.get(individual)
                #list3 = pop_sep3.get(individual)
                if list1[0] == p2:
                        list2 = list1[1:]
                        #list4 = list3[1:]
                        output.write(individual + " ,  ")
                        #output_not.write(individual + " ,  ")
                        t = 0
                        for x in list2:
                                t = t + 1
                                output.write(x)
                                if t < (len(list2)):
                                        output.write("\t")
                        output.write("\n")
                        #v = 0
                        #for x in list4:
                        #v = v + 1
                        #output_not.write(x)
                        #if v < (len(list4)):
                        #output_not.write("\t")
                        #output_not.write("\n")


output.close()
#output_not.close()


output2 = open(my_out2, "w")
output2 = open(my_out2, "a")
for x in locus_list_not:
        output2.write(x + "\n")
output2.close()


summary = open(my_out3, "w")
summary.write(str(out_ma) + "\tloci excluded due to minor allele frequency\n")
summary.write((str(thre_in/2)) +  "\tout of " + str(len(ind_names)) + " individuals required\n")
summary.write(str(out_in) + "\tloci excluded due to individual threshold\n")
if variable == "y":
        summary.write(str(out_var) + "\tloci excluded as invariable\n")
else:
        summary.write("variable option not set\n")
if two_all == "y":
        summary.write(str(out_two) + "\tloci excluded because they have more than two alleles\n")
else:
        summary.write("\ttwo allele option not set\n")
if only_one == "y":
        summary.write(str(old_len - len(locus_list_ok)) + "\tloci excluded as multiple SNPs passed per loucs\n")
else:
        summary.write("only one SNP per locus option not set\n")

summary.write(str(len(locus_list_ok)) + "\tout of " + str(len(locus_list))  + " loci recovered\n")

summary.close()
