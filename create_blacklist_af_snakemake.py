## Python script to create blacklist for Snakemake workflow identifying specimens not passing the filter criteria
## written by Martina Weiss, University of Duisburg-Essen
## used in Weiss et al 2017...

import pandas as pd
import os

#input files, defined in Snakefile
# lfmm: file created in rule create_snmf_files (o1) in Snakmake worflow for all specimens
# indi: file created in rule create_snmf_files (o2) in Snakmake worflow for all specimens
lfmm = pd.read_table(snakemake.input.lfmm, sep=" ", header=None)
indi = pd.read_table(snakemake.input.indi)
indi.columns = ["individual", "population"]
# reads: txt-file containing total number of loci for each individual (headers: individual, loci)
reads = pd.read_table(snakemake.input.reads)

#create blacklist of individuals with more than 15% missing loci and less than 80000 "starting" loci
df = pd.concat([indi, lfmm], axis = 1)
nb_loci = len(df.columns) - 2
df["nb_missing"] = df.apply(lambda row: sum(row[:]==9), axis=1)
df["perc_missing"] = df.nb_missing / nb_loci
df = df.loc[:, ["individual", "nb_missing", "perc_missing"]]
df = pd.merge(df, reads, how = "outer")
df["blacklist"] = 0
df.loc[(df["perc_missing"] > 0.15) | (df["loci"] < 80000), "blacklist"] = 1
df = df.round({"nb_missing":0, "perc_missing":2})
df_full = df.loc[:, ["individual", "site", "nb_missing", "perc_missing", "loci", "blacklist" ]]
df_final = df.loc[:, ["individual", "site", "blacklist"]]
#print output files
df_final.to_csv(snakemake.output.blacklist, sep = "\t")
df_full.to_csv(snakemake.output.missing, sep = "\t")
