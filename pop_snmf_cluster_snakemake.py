## Python script used to convert genepop file into lfmm file.
## written by Hannah Weigand, modified by Martina Weiss, University of Duisburg-Essen
## used in Weiss et al 2017...

import re
import os
import shutil
import sys

#Generate empty lists and dics

i = 0
pop = 0
loc_count = 0
div_list = []
ind_names = []
pop_sep = {}
stat = {}
loc_dic = {}
allele_dic = {}

#Get file and folder names:
input = snakemake.input[0]
output = snakemake.output.o1
output_ind = snakemake.output.o2
output_loc = snakemake.output.o3

print("Input file: " + input)
print("Output file: " + output)
print("Output list individuals: " + output_ind)

#Generate a list with population and genotypes per individual, a list of locus names,
#a list of individuals and the total number of individuals
for line in open(input):
        i = i + 1
        if i > 2:
                line2 = line.replace(" ","").replace(",", "\t").rstrip("\n").rstrip("\t").split("\t")
                if len(line2) < 2:
                        pop = pop + 1
                else:
                        pop_sep[line2[0]]=[pop]+line2[1:]
                        ind_names = ind_names + [line2[0]]
        if i == 2:
                locus_list = line.replace(" ","").rstrip("\n").split(",")

        numb_pop = pop
line3 = line2[1:]

#Per locus generate a list of different alleles

for ind in ind_names:
        line = pop_sep.get(ind)[1:]
        loc_pos = 0
        for loc in line:
                loc_pos = loc_pos + 1
                if list(loc_dic.keys()).count(loc_pos) < 1:
                        loc_dic[loc_pos] = [loc]
                else:
                        allele_list = loc_dic[loc_pos]
                        a1 = loc[0:2]
                        a2 = loc[2:]
                        loc_alt = a2 + a1
                        if (allele_list.count(loc)) < 1 and (allele_list.count(loc_alt) < 1):
                                loc_dic[loc_pos]=allele_list + [loc]



#See if the locus is invairable (i), missing data (m), too diverse (d) or variable (v)
#Variable loci can be transformed in lfmm format

for locus in loc_dic.keys():
        geno = loc_dic.get(locus)
        allele_list = []
        allele1 = ""
        allele2 = ""
        if len(geno) == 1 and geno.count("0000") == 1:
                div_list = div_list + ["n"]
        elif len(geno) == 1 and geno.count("0000") < 1:
                div_list = div_list + ["i"]
        elif len(geno) == 2 and geno.count("0000") == 1:
                div_list = div_list + ["i"]
        elif len(geno) == 5 and geno.count("0000") < 1:
                div_list = div_list + ["d"]
        elif len(geno) > 5:
                div_list = div_list + ["d"]
        else:
                for gen in geno:
                        allele1 = gen[0:2]
                        allele2 = gen[2:]
                        if allele_list.count(allele1) < 1:
                                allele_list = allele_list + [allele1]
                        if allele_list.count(allele2) < 1:
                                allele_list = allele_list + [allele2]

                if len(allele_list) == 2 and allele_list.count("00") == 1:
                        div_list = div_list + ["i"]
                elif len(allele_list) == 2 and allele_list.count("00")<1:
                        div_list = div_list + ["v"]
                        allele_list.sort()
                        allele_dic[locus] = allele_list
                elif len(allele_list) == 3 and allele_list.count("00")==1:
                        div_list = div_list + ["v"]
                        allele_list.sort()
                        allele_dic[locus] = allele_list
                else:
                        div_list = div_list + ["d"]





#Per individual use only variable loci
#Generate list of loci names

used_loci = ""
loc_dic2 = {}
incl_loci = []
excl_loci = []

for div in div_list:
        loc_count = loc_count + 1
        if div == "v":
                incl_loci = incl_loci + [loc_count]
                used_loci = used_loci + str(locus_list[loc_count-1]) + "\n"
                for ind in ind_names:
                        ind_list = pop_sep.get(ind)
                        ind_list1 = ind_list[1:]
                        if list(loc_dic2.keys()).count(ind)<1:
                                loc_dic2[ind] = []
                        loc_list = loc_dic2.get(ind) + [ind_list1[loc_count-1]]
                        loc_dic2[ind] = loc_list

        else:
                excl_loci = excl_loci + [loc_count]


#convert to lfmm
#save first occuring allele as A1

A1 = []
lfmm = {}
loci = list(allele_dic.keys())
loci.sort()
ind_names.sort()

for ind in ind_names:
        lfmm[ind] = []
        loc_count2 = 0
        for loc in loci:
                loc_count2 = loc_count2 + 1
                locus = loc_dic2.get(ind)[loc_count2 - 1]
                allele_list2 = allele_dic.get(loc)
                a1 = locus[0:2]
                a2 = locus[2:]
                if allele_list2[0]== "00":
                        allele1 = allele_list2[1]
                        allele2 = allele_list2[2]
                else:
                        allele1 = allele_list2[0]
                        allele2 = allele_list2[1]

                if a1 == allele1:
                       count = 1
                elif a1 == allele2:
                        count = 0
                else:
                        count = 9
                if a2 == allele1:
                        count = count + 1
                elif a2 == allele2:
                        count = count + 0
                else:
                        count = 9


                loc_list = lfmm.get(ind)
                lfmm[ind] = loc_list + [count]




#write files
data_lfmm = open(output, "w")
data_lfmm = open(output, "a")

for ind in ind_names:
        line = lfmm.get(ind)
        line2 = ""
        for loc in line:
                line2 = line2 + str(loc) + " "
        line2 = line2.rstrip(" ")
        data_lfmm.write(line2 + "\n")

data_lfmm.close()

data_locus = open(output_loc, "w")
data_locus.write(used_loci)
data_locus.close()


data_ind = open(output_ind, "w")
data_ind = open(output_ind, "a")

data_ind.write("Name\tPopulation\n")

for ind in ind_names:
        data_ind.write(ind + "\t")
        data_ind.write(str(pop_sep.get(ind)[0]) + "\n")

data_ind.close()

print("Convertion finished")
