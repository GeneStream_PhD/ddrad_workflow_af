## Python script used to divide specimens of genepop file into predefined populations
## written by Hannah Weigand, modified by Martina Weiss, University of Duisburg-Essen
## used in Weiss et al 2017...

import re
import os
import shutil
import sys
import numpy as np


my_data = snakemake.input[0]
my_pop = snakemake.input.l1
my_out = snakemake.output.o1
out_ass = snakemake.output.o2


#empty lists, tables, ...
i = 0
pop = 0
pop_sep = {}
ind_names = []
pop_assing = {}
new_pop_list = []
not_ass = 0

#read data and bring it to usable format
for line in open(my_data):
        i = i + 1
        if i == 1:
                title = line.rstrip("\n")
        elif i == 2:
                locus_list = line.replace(" ","").rstrip("\n").split(",")

        else:
                line2 = line.replace(" ","").replace(",", "\t").rstrip("\n").split("\t")
                if len(line2) < 2:
                        pop = pop + 1
                else:
                        pop_sep[line2[0]]=line2[1:]
                        ind_names = ind_names + [line2[0]]
                        pop_assing[line2[0]] = pop
        numb_pop = pop

#Summerize data
print(str(numb_pop)+ " population/s identified")

for line in open(my_pop):
        line3 = line.replace(" ", "\t").replace("\t\t", "\t").rstrip("\n").split("\t")
        ind = line3[0]
        old = pop_assing.get(ind)
        if old is None:
            continue

        if len(line3) > 1:
                new = line3[1]
                if new not in new_pop_list:
                        new_pop_list.append(new)
        else:
                new = "no population assigned"
                not_ass += 1
        pop_assing[ind] = [old, new]


print("divided into " + str(len(new_pop_list)) + " populations")
print(str(not_ass) + " individuals not assigned to any population")



#Write output
ind_names.sort()

output_pop = open(my_out, "w")
output_pop = open(my_out, "a")
output_pop.write(title + "\n")

print(*locus_list, sep=",", file=output_pop)


for p in new_pop_list:
        output_pop.write("POP\n")
        print("Output population " + str(p))
        for individual in ind_names:
                if pop_assing.get(individual)[1] == p:
                        list2 = pop_sep.get(individual)
                        output_pop.write(individual + " ,  ")
                        t = 0
                        for x in list2:
                                t = t + 1
                                output_pop.write(x)
                                if t < (len(list2)):
                                        output_pop.write("\t")
                        output_pop.write("\n")

output_pop.close()

output_ass = open(out_ass, "w")
output_ass = open(out_ass, "a")
output_ass.write("Individual\told population\tnew population")
written = False
for indi in ind_names:
        pop = pop_assing.get(indi)
        if isinstance(pop, list):
            written = True
            old, new = pop
            output_ass.write("\n" + str(indi) + "\t" + str(old) + "\t" + str(new))

assert written

output_ass.close()

print("formating finished")
