## Python script used to reformate genepop files obtained by stacks2fasta
## written by Hannah Weigand, modified by Martina Weiss, University of Duisburg-Essen
## used in Weiss et al 2017...

import re
import os
import shutil
import sys

do_single = snakemake.config["nb_snps"]
input_file = snakemake.input[0]
output_file = snakemake.output[0]
title_line = snakemake.input[0]

def parts(input_file):
        prev_line = ""
        loc_list = []
        pop_count = 0
        indi = {}
        for line in open(input_file):
                if line.startswith("<"):
                        prev_line = "Titel"
                elif line.startswith("locus"):
                        line2 = line.replace("locus stacks_", "").replace("<A>","").replace("\n", "").replace(".", "_").replace("\t","")
                        loc_list = loc_list + [line2]
                elif line.upper().startswith("POP") or line.upper().startswith("Pop") or line.upper().startswith("pop"):
                        pop_count = pop_count + 1
                else:
                        newline = line.replace("\t1\t\t", "\t").replace("[A]", "01").replace("[C]", "02").replace("[G]", "03").replace("[T]", "04").replace("[N]", "00").replace("[-]", "00").replace("[]", "00").replace(">","").replace("<","")
                        k = 0
                        for x in newline.rstrip("\n").split("\t"):
                                if k == 0:
                                        indivi = x
                                        indi[indivi] = [pop_count]
                                else:
                                        indi[indivi] = indi.get(indivi) + [x]
                                k = k + 1

        return(pop_count, loc_list, indi)

def exclude(loc_list,indi):
        incl_loc = []
        incl_numb = []
        indi2 = {}
        l = 1
        for x in loc_list:
                if x.endswith("_0") == True:
                	incl_loc = incl_loc + [x]
                	incl_numb = incl_numb + [l]
                	l = l + 1

        for ind in indi.keys():
                lo = indi.get(ind)
                indi2[ind] = [lo[0]]
                for locu in incl_numb:
                        indi2[ind] = indi2.get(ind) + [lo[locu]]
        return(incl_loc, indi2)



def rebuild_first(incl_loc,indi2, pop_count,output_file, title_line):
        output = open(output_file, "a")
        output = open(output_file, "w")
        output.write(title_line + "\n")
        indivi = list(indi2.keys())
        indivi.sort()
        for x in range(0,(len(incl_loc)-1)):
                a = incl_loc[x]
                output.write(a + ",")
        output.write(incl_loc[-1] + "\n")
        for p in range(1,(pop_count+1)):
                output.write("POP\n")
                for individual in indivi:
                        list1 = indi2.get(individual)
                        if list1[0] == p:
                                list2 = list1[1:]
                                output.write(individual + " ,  ")
                                t = 0
                                for x in list2:
                                        output.write(x)
                                        if t < (len(list2) - 1):
                                                output.write("\t")
                                output.write("\n")
        print("formating finished")
        output.close()



def rebuild_all(input_file, output_file, title_line):
        output = open(output_file, "a")
        output = open(output_file, "w")
        prev_line = ""
        for line in open(input_file):
                if line.startswith("<"):
                        output.write(title_line + "\n")
                        prev_line = "Titel"
                elif line.startswith("locus"):
                        line2 = line.replace("locus stacks_", "").replace("<A>","").replace("\n", "").replace("\t", ",").replace(".", "_")
                        output.write(line2)
                        prev_line = "Locus"
                elif line.upper().startswith("POP") or line.upper().startswith("Pop") or line.upper().startswith("pop"):
                        if prev_line == "Locus":
                                Pop = "\nPOP\n"
                        else:
                                Pop = "POP\n"
                        prev_line = "Pop"
                        output.write(Pop)
                else:
                        newline = line.replace("\t1\t\t", " ,  ").replace("[A]", "01").replace("[C]", "02").replace("[G]", "03").replace("[T]", "04").replace("[N]", "00").replace("[-]", "00").replace("[]", "00").replace(">","").replace("<","")
                        output.write(newline)
                        prev_line = "Ind"
        print("formating finished")
        output.close()


if do_single == "f":
        pop_count,loc_list,indi = parts(input_file)
        incl_loc, indi2 = exclude(loc_list,indi)
        m = re.search("/(.*).pop", title_line)
        if m:
            title_line = m.group(1)
            print(title_line)
        rebuild_first(incl_loc,indi2, pop_count,output_file, title_line)
        print("First snp was chosen")

elif do_single == "a":
        m = re.search("/(.*).pop", title_line)
        if m:
            title_line = m.group(1)
            print(title_line)
        rebuild_all(input_file, output_file, title_line)
        print("all snps were chosen")
