## R Script for Snakemake workflow to conduct a snmf cluster analysis
## written by Martina Weiss, University of Duisburg-Essen
## used in Weiss et al 2017...

library("LEA")

#Perform snmf
print("snmf will start")
mini = snakemake@config[["mini"]]
maxi = snakemake@config[["maxi"]]
rep = snakemake@config[["rep"]]
iter = snakemake@config[["iter"]]
ploid = snakemake@config[["ploidy"]]

min_cluster <- data.frame()
for (file in snakemake@input[["cluster"]]) {
  file2 = sub(".lfmm", ".snmfProject", file)
  file_MCE = sub(".lfmm", "_MCE.pdf", file)
  file_boxplot = sub(".lfmm", "_boxplot_MCE.pdf", file)
  if (file.exists(file2)) {
    snmf1 <- load.snmfProject(file2)
    name_dataset <- sub(".*gesamt_(.*_ma.)_.*", "\\1", file)
    cairo_pdf(filename = file_MCE)
    plot(snmf1, lwd=2, pch=20)
    dev.off()

    all_min = data.frame()
    all_med = data.frame()
    box_data = matrix(nrow=rep, ncol=(maxi-mini+1))
    all_best_runs = data.frame()

    for (x in 1:(maxi-mini+1)){
      K = (mini+x-1)
      ce = cross.entropy(snmf1, K = K)
      min = min(ce)
      best_run = which.min(ce)
      best_min = cbind(K, min, best_run)
      all_min = rbind(all_min, best_min)
      cluster_min = which.min(all_min$min)
      best_min_run = all_min$best_run[cluster_min]

      med = median(ce)
      best_med = cbind(K, med, best_run)
      all_med = rbind(all_med, best_med)
      cluster_med = which.min(all_med$med)
      best_med_run = all_min$best_run[cluster_med]

      best_runs = cbind(K, best_run)
      all_best_runs = rbind(all_best_runs, best_runs)

      for (y in 1:rep){
        box_data[y,x] = ce[y,]
      }
      par(mar=c(1,1,1,1))
      boxplot(box_data)
      cairo_pdf(filename = file_boxplot)
      boxplot(box_data, lwd=1, pch=20)
      dev.off()
    }
  }else {
    print(file)
    if (file.size(file) > 10000) {
      snmf1 <- snmf(file, K=mini:maxi, entropy=T, repetitions = rep, iterations = iter, ploidy = ploid, project = "new")
      name_dataset <- file
      name_dataset <- sub(".*gesamt_(.*_ma.)_.*", "\\1", name_dataset)
      cairo_pdf(filename = file_MCE)
      plot(snmf1, lwd=2, pch=20)
      dev.off()

      all_min = data.frame()
      all_med = data.frame()
      box_data = matrix(nrow=rep, ncol=(maxi-mini+1))
      all_best_runs = data.frame()

      for (x in 1:(maxi-mini+1)){
        K = (mini+x-1)
        ce = cross.entropy(snmf1, K = K)
        min = min(ce)
        best_run = which.min(ce)
        best_min = cbind(K, min, best_run)
        all_min = rbind(all_min, best_min)
        cluster_min = which.min(all_min$min)
        best_min_run = all_min$best_run[cluster_min]

        med = median(ce)
        best_med = cbind(K, med, best_run)
        all_med = rbind(all_med, best_med)
        cluster_med = which.min(all_med$med)
        best_med_run = all_min$best_run[cluster_med]

        best_runs = cbind(K = K, best_run = best_run)
        all_best_runs = rbind(all_best_runs, best_runs)

        for (y in 1:rep){
          box_data[y,x] = ce[y,]
        }
        par(mar=c(1,1,1,1))
        boxplot(box_data)
        cairo_pdf(filename = file_boxplot)
        boxplot(box_data, lwd=1, pch=20)
        dev.off()
      }
    }else {
      name_dataset <- file
      name_dataset <- sub(".*gesamt_(.*_ma.)_.*", "\\1", name_dataset)
      cluster_min = "NA"
      cluster_med = "NA"
      best_min_run = "NA"
      best_med_run = "NA"
      all_best_runs = "NA"
    }
  }
  cluster = cbind(dataset = name_dataset, K_min = cluster_min, R_best_min = best_min_run, K_med = cluster_med, R_best_med = best_med_run)
  min_cluster = rbind(min_cluster, cluster)
  print(min_cluster)
  cat(name_dataset, "\n", file = snakemake@output[["o2"]], sep = " ", append = TRUE)
  write.table(all_best_runs, file = snakemake@output[["o2"]], sep = "\t", row.names = FALSE, quote = FALSE, append = TRUE)
  cat("\n\n", file = snakemake@output[["o2"]], sep = " ", append = TRUE)
}

overall_stat = read.table(snakemake@input[["stats"]], header = TRUE, sep = "\t")
as = merge(overall_stat, min_cluster, by = "dataset", all = TRUE)
all_statistics = as[order(rev(as$clj), as$minaf, as$lim, as$nb_loci, decreasing = TRUE),]
print(all_statistics)

write.table(all_statistics, file = snakemake@output[["o1"]], sep = "\t", row.names = FALSE, quote = FALSE)

warnings()
